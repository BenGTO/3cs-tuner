[![pipeline status](https://gitlab.com/BenGTO/3cs-tuner/badges/main/pipeline.svg)](https://gitlab.com/BenGTO/3cs-tuner/-/commits/main) [![coverage report](https://gitlab.com/BenGTO/3cs-tuner/badges/main/coverage.svg?job=Unittests)](https://gitlab.com/BenGTO/3cs-tuner/-/commits/main)

# 3COMMAS DCA TP Tuner

Set take profit percentage depending on safety orders (SO) consumed.

By defining strategies, the take profit price (TP) of deals will be modified.
Strategies may be adopted the current market movement by adding an optional
scaling factor, and different tokens can have scaling factors to use the same
strategy for different kinds of tokens.

Say you run a couple of standard Trade Alts bot with 30 SO, and express bots with
increasing deviation but only 15 SO for selected tokens like ETH or ADA. The
different spread of SO requires two different strategies.

Define the strategies to work in a sideways moving market. For bullish markets,
add a strategy scaling of >1, for bearish markets a scaling of <1. A token with
unusual volatility can be further tuned by adding a deal scaling
factor.

The application is meant to be run continuously 24/7 in the background.
Either use a crontab entry or define the loop timer setting. With the
loop timer, the configuration will be reread and changes adopted for every
new cycle.

Tested with Python 3.7+.

# Basic Usage
```shell
> tune3cs --help
Usage: tune3cs [OPTIONS]

Options:
  -d, --debug TEXT        Logging verbosity (INFO, DEBUG, WARN)
  -l, --loop INTEGER      Loop permanently, wait [LOOP] seconds
  -c, --configfile TEXT   Path to configuration file
  -n, -dry-run            Show what would be done, doesn`t modify anything
  --help                  Show this message and exit.
```

## Versioning Conventions
Check the version of the installation with

```shell
tune3cs --version
```

The version number explained:

 1. Fix/enhancement releases (1.2.1 to 1.2.2): no changes in compatibility of configuration files.
 2. Minor releases (1.2.1 to 1.3.0): configuration items added, no compatibility issues. Optionally update your
   configuration to profit from enhancements.
 3. Major releases (1.2.1 to 2.0.0): Test before updating, there may be additions/removals of important configuration
   items or commandline options (check with `--dry-run`).

Updates are suitable for continuous integration except major releases.

# Installation
## With Python Package
```shell
pip install -U --user tune3cs --extra-index-url https://gitlab.com/api/v4/projects/28747308/packages/pypi/simple
tune3cs --configfile config.yml
```

## Docker
Place a configuration file `config.yml` in your current directory and run
```shell
docker run --rm --name tune3cs_test --volume="$PWD:/app/config" --env TUNE3CS_API_KEY --env TUNE3CS_API_SECRET registry.gitlab.com/bengto/3cs-tuner:latest -l 60 -c config.yml
```
Use a specific tag instead of `latest` for production!

If you want to create a container which runs permanently:

```shell
docker run --restart unless-stopped --name tune3cs_test --volume="$PWD:/app/config" --env TUNE3CS_API_KEY --env TUNE3CS_API_SECRET registry.gitlab.com/bengto/3cs-tuner:latest -l 60 -c config.yml
```


# Configuration

Configuration is in YAML format. See the configuration example here in the `tune3cs` directory.

- **strategies** define how a TP depends on the current SO
- **deals** list open deals with their TP tuning strategy

In loop mode, the configuration is read before each execution. If you apply
changes, watch the log for errors and warnings. Execution of deal tunings may
fail of single deals, or the job may stop entirely. Many configuration issues are
caught and processed to keep the tuner running, but you never know...

## 3COMMAS API Keys
You need to create an API key with *Bots read* and *Bots write* permissions.
Then set these environment variables:
- `TUNE3CS_API_KEY`
- `TUNE3CS_API_SECRET`

## Strategy
Defines how a deal has to be tuned. Define as many strategies as you need.

### Discrete Strategy
Set TP explicitly for each SO step. Increments should be adapted to the SOS defined for a bot.
```yaml
strategies:
  - name: standard
    type: discrete
    scaling: 1.5
    steps:
      - <value for BO>
      - <value for 1 SO>
      - <value for 2 SO>
      - <value for 3 SO>
  - name: constant
    type: discrete
    steps:
      - 2
```
Last value is used for subsequent SOs (means: no more changes).
The `constant` example strategy sets TP to 2% and leaves it there for
all conditions.

`scaling` is an optional scaling value, applied to all steps. I.e. if the trend goes from
bullish to bearish, scale down the TP ladder for all of your deals.

### Linear Strategy
Takes an initial TP and adds filled SO multiplied with a scaling factor:
```yaml
strategies:
  - name: linear_1
    type: linear
    initial_tp: 1
    scaling: 0.5
    max_tp: 5
```
would set TP to 1 at base order, 1.5 with 1 filled SO, 2 with 2 filled SO, 2.5 with 3 filled SO etc. TP will
never go higher than 5%.

`max_so` and `max_tp` are mutually exclusive and optional to set an upper TP limit. `max_so` sets the highest SO
filled to raise the TP, further SOs will no more affect TP. `max_tp` limits the maximum TP directly.

## Deals
```yaml
deals:
  - name: ETH/BUSD
    strategy: standard
    scaling: 0.75
  - name: ADA/BUSD
    strategy: standard
    disabled: 1
```
`name` is the name of the deal, obviously. Choose one of your defined strategies.
Optionally set `disabled` to skip tuning this deal.

`scaling` is optional and defaults to 1. This deal specific multiplier is applied to the TP.

## Scaling
Scaling helps to

- change the behavior of a group of bots with little editing.
- fine tune one strategy for individual deals with different volatility

### Strategy scaling
For discrete scaling, the multiplier is applied to every TP value in the list. For linear scaling, the scaling
is inherent to the definition.

### Deal scaling
Deals can be scaled individually. The deal scaling multiplier is applied to the TP resulting from the
strategy scaling.

Example: a discrete strategy which sets the TP at BO to 1 has a given scaling of 2. Deals without a scaling
parameter will be set to a TP of 2%. A deal scaling of 1.5 would rise the TP to 3%.

# How to run
Use the `--dry-run` option first to get an impression of what will be changed. None of
the changes will be applied to your deals.

The application should run 24 hours. A desktop is usually not an optimal environment.
A home server or NAS capable of running Python or Docker is a good start, or any hosted
system. Here are some ideas of how to run `tune3cs`.

## API Key Security
It's all about your money, keep your keys secure. If working on Linux, here are some ideas.

If you want to start `tune3cs` manually, store your keys by typing
```shell
read -s TUNE3CS_API_KEY
```
and entering your key. The `-s` hides your input, and nothing will be stored
in a file.

Some deployments require you to save your API keys to make them available to `tune3cs`.
If the file `/home/myuser/.tune3cs/.keys` holds your API key:
```shell
TUNE3CS_API_KEY=abcdef
TUNE3CS_API_SECRET=zyx123456
```
then protect it by making it readable for your user only with
```shell
chmod 600 /home/myuser/.tune3cs/.keys
```
Keep in mind that a malicious root user may read all of your files! This includes an
attacker gaining root permissions, so keep your system up to date, if you manage
it yourself.

Use a system exclusively for `tune3cs` if possible and disable all
network services, disable password login and use SSH key based authentication.

If you want to go further, read about port knocking or refrain to manual starts.

## Simple Background Job
On Linux, you may run `tune3cs` in a terminal. To avoid your job being suspended when you
close the terminal, you can use `screen`:
```shell
screen tune3cs -l 60 -c config.yml
```
Detach the session with *CTRL-A d*. The job continues even if you log out.
To reattach, type
```shell
screen -x
```
To stop the job, attach the session and type *CTRL-C*. The job stops and the
screen session will be closed.

## Crontab Entry
If you have the permission to create a crontab, you may add an entry with
`crontab -e` and editing the crontab:
```
* * * * * . /home/myuser/.tune3cs/.keys && tune3cs -c /home/myuser/.tune3cs/config.yml
```
This will run `tune3cs` every minute.

## Docker Compose
TBD

## Kubernetes
TBD

# Development
The code will be checked by *pre-commit*. To check locally, install it with `pip`,
either globally or locally:

```shell
pip install pre-commit
pre-commit install

pre-commit run --all-files
```
