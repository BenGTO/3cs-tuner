FROM python:3-alpine3.9
# ARG VCS_TAG

LABEL maintainer="Bengt Giger <beni@directbox.com>"
ENV PYTHONUNBUFFERED=1

COPY tune3cs /app/tune3cs
COPY requirements.txt /app/

WORKDIR /app
RUN pip install -r requirements.txt
# RUN echo $VCS_TAG >.version.txt

RUN addgroup --system py && adduser --system --ingroup py py
USER py:py

# CMD python3 -m tune3cs --configfile=config.yml
ENTRYPOINT ["python3", "-m", "tune3cs", "--configfile", "config.yml"]
