from setuptools import setup, find_packages
import tune3cs

setup(
    name="tune3cs",
    keywords="api 3commas trading crypto bitcoin altcoin",
    version=tune3cs.__version__,
    py_modules=["3cs-tuner"],
    packages=find_packages(),
    include_package_data=True,
    install_requires=["Click", "pyyaml", "py3cw"],
    entry_points={
        "console_scripts": [
            "tune3cs = tune3cs.tuner:tune",
        ],
    },
)
