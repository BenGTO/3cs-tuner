import unittest
import tempfile
import os
import yaml
import logging
import tune3cs.strategy
import tune3cs.deals
from tune3cs.config import Config
from tune3cs.connector import DummyConnector
from tune3cs.deals import OpenDeals, DefinedDeals
from tune3cs.deal import Deal

test_configuration = """
deals:
  - name: discrete1_low
    strategy: discrete1
  - name: discrete1_medium
    strategy: discrete1
  - name: discrete1_high
    strategy: discrete1
  - name: scaling1_low
    strategy: scaling1
  - name: scaling1_medium
    strategy: scaling1
  - name: scaling1_high
    strategy: scaling1
  - name: scaling2_low
    strategy: scaling2
  - name: scaling2_medium
    strategy: scaling2
  - name: scaling2_high
    strategy: scaling2
  - name: local_scaling_low
    strategy: linear1
    scaling: 2
  - name: local_scaling_medium
    strategy: linear1
    scaling: 2
  - name: local_scaling_high
    strategy: linear1
    scaling: 2
strategies:
  - name: discrete1
    type: discrete
    steps:
      - 1
      - 2
      - 3
      - 4
  # non numeric
  - name: discrete_invalid1
    type: discrete
    steps:
      - 1
      - a
      - 3
  # negative scaling
  - name: discrete_scaling_invalid2
    type: discrete
    scaling: -1
    steps:
      - 1
      - 2
      - 3
  # non numeric
  - name: discrete_scaling_invalid3
    type: discrete
    scaling: abc
    steps:
      - 1
      - 2
      - 3
  - name: scaling1
    type: discrete
    scaling: 0.5
    steps:
      - 1
      - 2
      - 3
  - name: scaling2
    type: discrete
    scaling: 2
    steps:
      - 2
      - 3
      - 4

  - name: linear_max_so
    type: linear
    scaling: 0.5
    initial_tp: 1
    max_so: 6
  - name: linear_max_tp
    type: linear
    scaling: 0.5
    initial_tp: 1
    max_tp: 2

  - name: linear_constant
    type: linear
    scaling: 0
    initial_tp: 0.5
  # no attributes
  - name: linear1_invalid
    type: linear
  # initial TP missing
  - name: linear2_invalid
    type: linear
    scaling: 2
  # scaling missing
  - name: linear3_invalid
    type: linear
    initial_tp: 2
  # non numeric
  - name: linear4_invalid
    type: linear
    initial_tp: 2
    scaling: a
  # non numeric
  - name: linear5_invalid
    type: linear
    initial_tp: a
    scaling: 2
  # negative scaling
  - name: linear6_negative
    type: linear
    initial_tp: 1
    scaling: -2
  # both max_so and max_tp
  - name: linear7_invalid
    type: linear
    initial_tp: 1
    scaling: 1
    max_so: 1
    max_tp: 1
"""
test_deals = [
    {"bot_name": "discrete1_low", "take_profit": 2, "deal_id": 123456, "so": 0},
    {"bot_name": "discrete1_medium", "take_profit": 2, "deal_id": 123456, "so": 1},
    {"bot_name": "discrete1_high", "take_profit": 2, "deal_id": 123456, "so": 5},
    {"bot_name": "scaling1_low", "take_profit": 2, "deal_id": 123456, "so": 0},
    {"bot_name": "scaling1_medium", "take_profit": 2, "deal_id": 123456, "so": 1},
    {"bot_name": "scaling1_high", "take_profit": 2, "deal_id": 123456, "so": 5},
    {"bot_name": "scaling2_low", "take_profit": 2, "deal_id": 123456, "so": 0},
    {"bot_name": "scaling2_medium", "take_profit": 2, "deal_id": 123456, "so": 1},
    {"bot_name": "scaling2_high", "take_profit": 2, "deal_id": 123456, "so": 5},
    {"bot_name": "linear1_low", "take_profit": 2, "deal_id": 123456, "so": 0},
    {"bot_name": "linear1_medium", "take_profit": 2, "deal_id": 123456, "so": 3},
    {"bot_name": "linear1_high", "take_profit": 2, "deal_id": 123456, "so": 8},
    {"bot_name": "local_scaling_low", "take_profit": 2, "deal_id": 123456, "so": 0},
    {"bot_name": "local_scaling_medium", "take_profit": 2, "deal_id": 123456, "so": 2},
    {"bot_name": "local_scaling_high", "take_profit": 2, "deal_id": 123456, "so": 8},
]


# Basic parsing tests
class TestStrategyBasicParsing(unittest.TestCase):
    def test_no_name_strategy(self):
        strategies = """
strategies:
    - type: "discrete"
      steps:
        - 1
"""
        tune3cs.strategy.parse(yaml.safe_load(strategies)["strategies"])
        with self.assertLogs() as lg:
            logging.getLogger().error("Strategy found without name, skipped")
        self.assertEqual(lg.output, ["ERROR:root:Strategy found without name, skipped"])

    def test_no_type_strategy(self):
        strategies = """
strategies:
    - name: "test"
      steps:
        - 1
"""
        tune3cs.strategy.parse(yaml.safe_load(strategies)["strategies"])
        with self.assertLogs() as lg:
            logging.getLogger().error("Strategy test has no type, skipped")
        self.assertEqual(lg.output, ["ERROR:root:Strategy test has no type, skipped"])

        def test_no_steps_strategy(self):
            strategies = """
    strategies:
        - name: "test"
          type: "discrete"
    """
            tune3cs.strategy.parse(yaml.safe_load(strategies)["strategies"])
            with self.assertLogs() as lg:
                logging.getLogger().error("Steps missing in strategy test, skipped")
            self.assertEqual(
                lg.output, ["ERROR:root:Steps missing in strategy test, skipped"]
            )


# Tests with configuration read from file
class TestStrategy(unittest.TestCase):
    def setUp(self) -> None:
        self.f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        self.f.write(test_configuration)
        self.f.close()

        self.config = Config(self.f.name)
        self.config.open()
        self.connector = DummyConnector()
        self.connector.connect(self.config, test_deals)
        self.openDeals = OpenDeals(self.config, self.connector)
        for deal in test_deals:
            self.openDeals.append(
                Deal(
                    deal["bot_name"],
                    deal["take_profit"],
                    deal_id=deal["deal_id"],
                    current_so=deal["so"],
                )
            )
        self.definedDeals = DefinedDeals(self.config)

    def tearDown(self) -> None:
        os.remove(self.f.name)


class TestStrategyElimination(TestStrategy):
    # Test all if strategy instances are (in)validated
    def test_members(self):
        self.assertNotIn("scaling_invalid1", self.config.strategies)
        self.assertNotIn("discrete_scaling_invalid2", self.config.strategies)
        self.assertNotIn("discrete_scaling_invalid3", self.config.strategies)
        self.assertIn("scaling1", self.config.strategies)
        self.assertIn("scaling2", self.config.strategies)
        self.assertIn("linear_max_so", self.config.strategies)
        self.assertIn("linear_max_tp", self.config.strategies)
        self.assertNotIn("linear1_invalid", self.config.strategies)
        self.assertNotIn("linear2_invalid", self.config.strategies)
        self.assertNotIn("linear3_invalid", self.config.strategies)
        self.assertNotIn("linear4_invalid", self.config.strategies)
        self.assertNotIn("linear5_invalid", self.config.strategies)
        self.assertIn("linear6_negative", self.config.strategies)
        self.assertNotIn("linear7_invalid", self.config.strategies)


class TestDiscreteStrategy(TestStrategy):
    # Test configuration parsing directly
    def test_no_steps_strategy(self):
        strategies = """
    strategies:
        - name: "test"
          type: "discrete"
    """
        tune3cs.strategy.parse(yaml.safe_load(strategies)["strategies"])
        with self.assertLogs() as lg:
            logging.getLogger().error("Steps missing in strategy test, skipped")
        self.assertEqual(
            lg.output, ["ERROR:root:Steps missing in strategy test, skipped"]
        )

    def test_invalid_steps_strategy(self):
        strategies = """
    strategies:
        - name: "test"
          type: "discrete"
          steps:
            - a
    """
        tune3cs.strategy.parse(yaml.safe_load(strategies)["strategies"])
        with self.assertLogs() as lg:
            logging.getLogger().error("Invalid step value in strategy test, skipped")
        self.assertEqual(
            lg.output, ["ERROR:root:Invalid step value in strategy test, skipped"]
        )

    #
    def test_discrete_standard(self):
        deal = self.openDeals.find(Deal("discrete1_low"))
        deal.modify(self.config.strategies["discrete1"])
        self.assertEqual(1, deal.new_tp)
        deal = self.openDeals.find(Deal("discrete1_medium"))
        deal.modify(self.config.strategies["discrete1"])
        self.assertEqual(2, deal.new_tp)
        deal = self.openDeals.find(Deal("discrete1_high"))
        deal.modify(self.config.strategies["discrete1"])
        self.assertEqual(4, deal.new_tp)

    def test_discrete_scaling1(self):
        deal = self.openDeals.find(Deal("scaling1_low"))
        deal.modify(self.config.strategies["scaling1"])
        self.assertEqual(1, deal.new_tp)
        deal = self.openDeals.find(Deal("scaling1_medium"))
        deal.modify(self.config.strategies["scaling1"])
        self.assertEqual(1, deal.new_tp)
        deal = self.openDeals.find(Deal("scaling1_high"))
        deal.modify(self.config.strategies["scaling1"])
        self.assertEqual(1.5, deal.new_tp)

    def test_discrete_scaling2(self):
        deal = self.openDeals.find(Deal("scaling2_low"))
        deal.modify(self.config.strategies["scaling2"])
        self.assertEqual(4, deal.new_tp)
        deal = self.openDeals.find(Deal("scaling2_medium"))
        deal.modify(self.config.strategies["scaling2"])
        self.assertEqual(6, deal.new_tp)
        deal = self.openDeals.find(Deal("scaling2_high"))
        deal.modify(self.config.strategies["scaling2"])
        self.assertEqual(8, deal.new_tp)


class TestLinearStrategy(TestStrategy):
    def check_tp(self, deal, expected, strategy, assert_equal=True):
        active_deal = self.openDeals.find(deal)
        active_deal.modify(strategy)
        if assert_equal:
            self.assertEqual(expected, active_deal.new_tp)
        else:
            self.assertNotEqual(expected, active_deal.new_tp)

    def select(self, name, expected):
        for d in self.definedDeals.data:
            if d.name == name:
                deal = Deal(d.name, strategy=d.strategy, scaling=d.scaling)
                TestLinearStrategy.check_tp(
                    self, d, expected, self.config.strategies["linear_max_so"]
                )
                return
        # Should never get here
        self.assertTrue(False)

    def test_local_scaling_high(self):
        TestLinearStrategy.select(self, "local_scaling_low", 2)
        TestLinearStrategy.select(self, "local_scaling_medium", 4)
        TestLinearStrategy.select(self, "local_scaling_high", 8)

    def test_linear_max_so(self):
        deal = self.openDeals.find(Deal("linear1_low"))
        deal.modify(self.config.strategies["linear_max_so"])
        self.assertEqual(1, deal.new_tp)
        deal = self.openDeals.find(Deal("linear1_medium"))
        deal.modify(self.config.strategies["linear_max_so"])
        self.assertEqual(2.5, deal.new_tp)
        deal = self.openDeals.find(Deal("linear1_high"))
        deal.modify(self.config.strategies["linear_max_so"])
        self.assertEqual(4, deal.new_tp)

    def test_linear_max_tp(self):
        deal = self.openDeals.find(Deal("linear1_low"))
        deal.modify(self.config.strategies["linear_max_tp"])
        self.assertEqual(1, deal.new_tp)
        deal = self.openDeals.find(Deal("linear1_high"))
        deal.modify(self.config.strategies["linear_max_tp"])
        self.assertEqual(2, deal.new_tp)

    def test_linear_constant(self):
        deal = self.openDeals.find(Deal("linear1_high"))
        deal.modify(self.config.strategies["linear_constant"])
        self.assertEqual(0.5, deal.new_tp)
