import logging
import collections
from tune3cs.deal import Deal

# Maximum deals retrieved from API
deals_limit = 100


def parse(deals_list, config):
    config.deals = Deals(config)
    for item in deals_list:
        if "name" not in item.keys():
            logging.error("Deal name missing, skipped")
            continue
        if "strategy" not in item.keys():
            logging.error("Strategy missing for deal {0}, skipped".format(item["name"]))
            continue

        if "scaling" in item.keys():
            try:
                scaling = float(item["scaling"])
            except ValueError as e:  # noqa: F841
                logging.error("Non numeric scaling in deal {0}".format(item["name"]))
                continue
            # Some people actually want negative scaling
            # if float(item["scaling"]) < 0:
            #     logging.error("Negative scaling in deal {0}".format(item["name"]))
            #     continue
        else:
            scaling = 1

        # Append deal if not disabled
        if "disabled" not in item.keys():
            # should not fail after all tests have passed
            deal = Deal(item["name"], strategy=item["strategy"], scaling=scaling)
            if deal is not None:  # pragma: no cover
                config.deals.append(deal)


class Deals(collections.UserList):
    """
    Base class for a list of deals
    """

    config = None

    def __init__(self, config):
        super().__init__()
        self.config = config

    def find(self, deal):
        """
        Compare a deal with deals in this list. Comparison is done by name attribute.
        :param deal: deal to be searched
        :return: deal found or None if not found
        """
        for i in range(len(self.data)):
            if self[i].name == deal.name:
                self[i].scaling = deal.scaling
                return self[i]
        return None

    def dump(self):
        for deal in self.data:  # pragma: no cover
            logging.debug(deal.name)  # pragma: no cover


class OpenDeals(Deals):
    """
    List of deals currently active deals from 3COMMAS
    """

    connector = None

    def __init__(self, config, connector):
        """
        Prepare deals list and connection to 3COMMAS API
        :param config: initialized configuration object from tune3cs.config
        """
        super().__init__(config)
        self.connector = connector

    def get(self):
        """
        Get the list of deals
        :return: None
        """
        deals = self.connector.get()
        if deals is None:  # pragma: no cover
            return  # pragma: no cover

        for deal in deals:
            if deal["status"] == "bought":
                self.append(
                    Deal(
                        deal["bot_name"],
                        deal["take_profit"],
                        deal_id=deal["id"],
                        current_so=deal["completed_safety_orders_count"],
                    )
                )
        logging.info("Found {0} active running deals".format(len(self)))

    def put(self):
        """
        Update active deals with new TP values
        :return: None
        """
        self.connector.put(self.data)  # pragma: no cover


class DefinedDeals(Deals):
    """
    Deals defined in configuration file
    """

    def __init__(self, config):
        """
        List of deals from configuration file
        :param config: initialized configuration object from tune3cs.config
        """
        super().__init__(config)
        for item in config.deals:
            self.append(item)

        logging.info("Found {0} active defined deals".format(len(self)))
