import logging


strategy_names = ["discrete", "linear"]


def parse(strategy_list):
    strategies = {}
    for item in strategy_list:
        try:
            name = item["name"]
        except KeyError as e:  # noqa: F841
            logging.debug("Strategy found without name, skipped")
            continue
        try:
            if item["type"] not in strategy_names:
                logging.error("Unknown strategy found")
                continue
        except KeyError as e:  # noqa: F841
            logging.error("Strategy {0} has no type, skipped".format(name))
            continue

        s = None
        if item["type"] == "discrete":
            scaling = 1
            if "scaling" in item.keys():
                scaling = item["scaling"]
            if "steps" not in item.keys():
                logging.error("Steps missing in strategy {0}, skipped".format(name))
                continue

            try:
                s = DiscreteStrategy(item["name"], item["steps"], scaling)
            except:  # nosec # noqa: E722
                # strategy is None and will be dropped
                continue

        if item["type"] == "linear":
            if "scaling" not in item.keys():
                logging.error("Scaling missing in strategy {0}".format(item["name"]))
                continue
            else:
                scaling = item["scaling"]

            if "initial_tp" not in item.keys():
                logging.error("Initial TP missing in strategy {0}".format(item["name"]))
                continue

            max_so = -1
            if "max_so" in item.keys():
                max_so = item["max_so"]
            max_tp = -1
            if "max_tp" in item.keys():
                max_tp = item["max_tp"]

            try:
                s = LinearStrategy(
                    item["name"],
                    item["initial_tp"],
                    scaling=scaling,
                    max_so=max_so,
                    max_tp=max_tp,
                )
            except:  # nosec # noqa: E722
                continue

        if s is not None:  # pragma: no cover
            strategies[item["name"]] = s

    return strategies


class Strategy(object):
    """
    Base class for strategies: return TP for a given safety order_volume step
    """

    def __init__(self, name):
        self.name = name

    def tp(self, step, scaling=1):
        """
        Return TP for a given safety order_volume step
        :param step: safety orders bought by deal
        :param scaling: optional scaling for this deal. Applied in addition to strategy scaling.
        :return: take profit value for current step
        """
        raise NotImplementedError  # pragma: no cover


class DiscreteStrategy(Strategy):
    """
    TP values listed, starting with no SO, base order_volume only.

    SO levels exceeding the list continue to use the highest value defined.
    """

    steps: []
    scaled_steps: []
    scaling: 1

    def __init__(self, name, steps, scaling=1):
        """
        Initialize strategy, setting name and TP list
        :param name: name defined in configuration
        :param steps: list of TP values
        :param scaling: scale TP values. Default = 1, optional
        """
        super().__init__(name)
        self.name = name

        # Make sure all values are numbers, otherwise this strategy will be skipped
        try:
            self.scaling = float(scaling)
        except ValueError as e:  # noqa: F841
            logging.error("Invalid scaling value in strategy {0}: skipped".format(name))
            raise e
        # Zero scaling is meaningless
        if float(scaling) <= 0:
            logging.error(
                "Invalid scaling value <=0 in strategy {0}: skipped".format(name)
            )
            raise ValueError

        scaled_steps = []
        try:
            base_tp = float(steps[0])
        except ValueError as e:  # noqa: F841
            logging.error("Invalid step value in strategy {0}: skipped".format(name))
            return
        for step in steps:
            try:
                float(step)
            except ValueError as e:  # noqa: F841
                logging.error(
                    "Invalid step value in strategy {0}: skipped".format(name)
                )
                return
            scaled_steps.append(max(base_tp, step * self.scaling))
        self.steps = scaled_steps

    def tp(self, step, scaling=1):
        """
        Return TP for a given safety order_volume step
        :param step: safety orders bought by deal
        :param scaling: optional scaling for this deal, currently unused
        :return: take profit value for current step
        """
        #  Go up steps, apply every step until current level is reached
        tp = 0
        test_step = 0
        for s in self.steps:
            if test_step <= step:
                tp = s
            test_step += 1

        return tp * scaling


class LinearStrategy(Strategy):
    """
    Strategy idea from Steward: I took the total amount of open orders (BO + SOs)
    and then divided by two to calculate a new TP. Example: 10 total open trades = 5% revised TP
    """

    start_tp: float
    scaling: float
    max_so: float
    max_tp: float

    def __init__(self, name, start_tp, scaling=1, max_so=-1, max_tp=-1):
        """
        Initialize linear strategy.
        :param name: strategy name
        :param start_tp: TP setting for BO only, no SO
        :param scaling: linear gradient, increment of TP per SO
        :param max_so: maximum SO used for calculation. Further SO filled have no effect on TP
        :param max_tp: maximum possible TP. Mutually exclusive with max_so
        """
        super().__init__(name)

        if max_so > -1 and max_tp > -1:
            logging.error("max_so and max_tp are mutually exclusive")
            raise KeyError

        try:
            self.start_tp = float(start_tp)
            self.scaling = float(scaling)
            self.max_so = float(max_so)
            self.max_tp = float(max_tp)
        except ValueError as e:
            logging.error("Non numeric value in strategy {0}".format(name))
            raise e

        # Seems some people actually want negative scales
        # if scaling < 0:
        #     logging.error("Negative scaling not allowed in {0}".format(name))
        #     raise ValueError

    def tp(self, step, scaling=1):
        # calculate TP based on SOs taken
        if self.max_so > -1:
            if step < self.max_so:
                active_step = step
            else:
                active_step = self.max_so
            return round((self.start_tp + active_step * self.scaling) * scaling, 2)

        # calculate TP based on maximum TP
        tp = round((self.start_tp + step * self.scaling) * scaling, 2)
        if self.max_tp > -1:
            if tp < self.max_tp:
                return tp
            else:
                return self.max_tp

        return tp
