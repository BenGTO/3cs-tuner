import tune3cs.config as c
from tune3cs.connector import ApiConnector
import tune3cs.deals as deals
from tune3cs import __version__
import logging
import time
import click


class Tuner(object):
    config = None
    connector = None

    def __init__(self, config_file, dry_run=False):
        """
        Initialize tuner and set default configuration file
        :param config_file: path to configuration file
        :param dry_run: do not write values back to 3COMMAS
        """
        self.config = c.Config(config_file)
        try:
            self.config.open()
        except Exception as e:  # noqa: F841
            # problem has already been logged, just exit
            exit(1)

        self.connector = ApiConnector(dry_run=dry_run)
        self.connector.connect(self.config)

        # Fetch active deals
        self.openDeals = deals.OpenDeals(self.config, self.connector)
        self.openDeals.get()
        # Create defined deals from configuration file
        self.definedDeals = deals.DefinedDeals(self.config)
        self.modifiedDeals = deals.OpenDeals(self.config, self.connector)

    def tune(self):
        """
        Walk through list of defined deals, search in active deals.
        If found, store in list of modified deals
        :return: None
        """
        for i in range(len(self.definedDeals)):
            deal = self.openDeals.find(self.definedDeals[i])
            if deal is not None:
                # Apply strategy defined in deal configuration
                try:
                    deal.modify(self.config.strategies[self.definedDeals[i].strategy])
                except KeyError as e:  # noqa: F841
                    logging.error(
                        "Deal {0} wants unknown strategy {1}: skipped".format(
                            deal.name, deal.strategy
                        )
                    )
                    continue
                self.modifiedDeals.append(deal)
        if len(self.modifiedDeals) == 0:
            logging.info("No matching deal found")

    def apply(self):
        """
        Push new TPs to 3COMMAS
        :return: None
        """
        try:
            self.modifiedDeals.put()
        except:  # noqa: E722
            logging.warning("Pushing new TPs failed")

    def show(self):
        """
        Dump modified deals for debugging
        :return: None
        """
        for d in self.modifiedDeals:
            print(d.name, d.tp)


@click.command()
@click.option(
    "--debug",
    "-d",
    default="INFO",
    show_default=True,
    help="Logging verbosity (INFO, DEBUG, WARN)",
)
@click.option("--debug_level", default="NONE", help="DEPRECATED: use --debug")
@click.option("--loop", "-l", default=0, help="Loop permanently, wait [LOOP] seconds")
@click.option(
    "--configfile", "-c", default="config.yml", help="Path to configuration file"
)
@click.option(
    "--dry-run",
    "-n",
    type=bool,
    is_flag=True,
    default=False,
    help="Show what would be done",
)
@click.option(
    "--version",
    "-v",
    type=bool,
    is_flag=True,
    default=False,
    help="Show version and exit",
)
def tune(debug, debug_level, configfile, loop=0, dry_run=False, version=False):
    if version:
        click.echo("tune3cs v{0}".format(__version__))
        exit(0)
    if debug_level != "NONE":
        logging.basicConfig(level=debug_level)
        logging.warning("--debug_level is deprecated, use --debug")
    else:
        logging.basicConfig(level=debug)

    logging.info("Starting tune3cs v{0}".format(__version__))
    if dry_run:
        logging.info("Running in dry run mode, no deal will be modified")

    if loop > 0:
        # Loop forever
        while True:
            t = Tuner(config_file=configfile, dry_run=dry_run)
            # Calculate new state
            t.tune()
            # Apply new state
            t.apply()
            logging.info(
                "{0} Next run in {1}s".format(
                    time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), loop
                )
            )
            # Destroy configuration, will be read again upon next loop
            del t
            time.sleep(loop)
    else:
        # One shot run
        t = Tuner(config_file=configfile, dry_run=dry_run)
        t.tune()
        t.apply()


if __name__ == "__main__":
    tune()
