from py3cw.request import Py3CW
import logging
import time

deals_limit = 100


class Connector(object):
    connection = None
    dry_run = False
    deals_list = None

    def __init__(self, dry_run=False):
        self.dry_run = dry_run

    def connect(self, config, deals_list=None):
        """
        Initialize connector
        :param config: configuration object
        :param deals_list: deals list, for testing purposes
        :return:
        """
        raise NotImplementedError

    def get(self):
        """
        Get the list of deals
        :return: raw list of deals from API
        """
        raise NotImplementedError

    def put(self, deals):
        """
        Send a list of deal objects through the connector
        :param deals: list of deal objects (deals.deals)
        :return:
        """
        raise NotImplementedError


class DummyConnector(Connector):
    def connect(self, config, deals_list=None):
        if deals_list is None or deals_list == []:
            logging.error("Missing deals list for dummy connector initialization")
            raise ValueError
        self.deals_list = deals_list

    def get(self):
        return self.deals_list

    def put(self, deals):
        return


class ApiConnector(Connector):
    def __init__(self, dry_run=False):
        super().__init__(dry_run)

    def connect(self, config, deals_list=None):
        """
        Connect to 3COMMAS api
        :param config: configuration object
        :param deals_list: deals list, not used for API connection
        :return:
        """
        try:
            self.connection = Py3CW(
                key=config.api_key,
                secret=config.api_secret,
                request_options={
                    "request_timeout": config.request_timeout,
                    "nr_of_retries": config.nr_of_retries,
                    "retry_status_codes": config.retry_status_codes,
                },
            )
        except ValueError as e:  # noqa: F841
            logging.error("Please enter a 3commas API secret")
            exit(1)

    def get(self):
        """
        Get the list of deals
        :return: raw list of deals from API
        """
        try:
            error, data = self.connection.request(
                entity="deals",
                action="",
                payload={"limit": deals_limit, "scope": "active"},
            )
        except UnboundLocalError as e:  # noqa: F841
            logging.error("Connection to 3COMMAS failed, response was empty.")
            return None

        if len(error.keys()) > 0:
            logging.error(error)

        logging.debug("Total number of raw deals: {0}".format(len(data)))
        return data

    def put(self, deals):
        for deal in deals:
            if deal.new_tp != deal.tp:
                logging.info("{0} {1} -> {2}".format(deal.name, deal.tp, deal.new_tp))
                if not self.dry_run or self.deals_list is not None:
                    error, data = self.connection.request(
                        entity="deals",
                        action="update_deal",
                        action_id=str(deal.deal_id),
                        payload={"take_profit": deal.new_tp},
                    )
                    if len(error.keys()) > 0:
                        logging.error(error)
                time.sleep(0.5)
