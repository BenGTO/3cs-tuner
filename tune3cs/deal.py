import logging


class Deal(object):
    """
    Deal object
    """

    tp = 0
    new_tp = 0
    name = ""
    current_so = 0
    scaling = 1
    strategy = None

    deal_id = -1

    def __init__(
        self, name, tp=0, new_tp=0, strategy="", deal_id=-1, current_so=0, scaling=1
    ):
        """
        Create a deal object
        :param name: name of the deal in 3COMMAS
        :param tp: current take profit value
        :param new_tp: new TP
        :param strategy: strategy used to tune TP
        :param deal_id: deal ID from 3COMMAS
        :param current_so: current SO
        """
        self.tp = float(tp)
        self.new_tp = new_tp
        self.name = name
        self.current_so = current_so
        self.deal_id = deal_id
        self.strategy = strategy
        self.scaling = float(scaling)

    def modify(self, strategy):
        """
        Set the new TP based on strategy
        :param strategy: strategy object
        :return: None
        """
        self.new_tp = strategy.tp(self.current_so, self.scaling)
        logging.debug(
            "modify: set {0} tp from {1} to {2}".format(self.name, self.tp, self.new_tp)
        )
