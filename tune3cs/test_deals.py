import unittest
import tempfile
import yaml
import logging
import tune3cs.config as config
import tune3cs.deal as deal
import tune3cs.deals as deals
from tune3cs.connector import ApiConnector

base_config = """
---
deals:
  - name: ADA/BUSD
    strategy: std
  - name: ETH/BUSD
    strategy: std
  - name: XRP/BUSD
    strategy: std
  - name: AXS/BUSD
    strategy: std
  - name: XLM/BUSD
    strategy: const
  - name: scaling_override1_invalid
    strategy: linear
    scaling: a
  - name: scaling_override2_invalid
    strategy: linear
    scaling: -1
  - name: scaling_override1_valid
    strategy: linear
    scaling: 2
  - name: disabled
    disabled: true
    strategy: discrete
    steps:
    - 1
strategies:
  - name: std
    type: discrete
    steps:
      - 1
      - 2
      - 3
      - 4
  - name: const
    type: discrete
    steps:
      - 3
  - name: linear
    type: linear
    initial_tp: 1
    scaling: 1
"""
test_deals = [
    {"bot_name": "ADA/BUSD", "take_profit": 2, "deal_id": 123456, "so": 2},
    {"bot_name": "XRP/BUSD", "take_profit": 2, "deal_id": 123456, "so": 0},
    {"bot_name": "AXS/BUSD", "take_profit": 2, "deal_id": 123456, "so": 6},
    {"bot_name": "XLM/BUSD", "take_profit": 2, "deal_id": 123456, "so": 3},
    {"bot_name": "BTC/BUSD", "take_profit": 2, "deal_id": 1234567, "so": 2},
    {
        "bot_name": "scaling_override1_invalid",
        "take_profit": 2,
        "deal_id": 1234567,
        "so": 2,
    },
    {
        "bot_name": "scaling_override2_invalid",
        "take_profit": 2,
        "deal_id": 1234567,
        "so": 2,
    },
    {
        "bot_name": "scaling_override1_valid",
        "take_profit": 2,
        "deal_id": 1234567,
        "so": 2,
    },
]


class MockupConnector(ApiConnector):
    def get(self):
        return [
            {
                "status": "bought",
                "bot_name": "test1",
                "take_profit": "2",
                "id": "123",
                "completed_safety_orders_count": "2",
            },
            {
                "status": "bought",
                "bot_name": "test2",
                "take_profit": "2",
                "id": "123",
                "completed_safety_orders_count": "2",
            },
            {
                "status": "ready",
                "bot_name": "test3",
                "take_profit": "2",
                "id": "123",
                "completed_safety_orders_count": "2",
            },
        ]


class TestParsing(unittest.TestCase):
    def test_missing_name(self):
        data = """
deals:
- strategy: discrete
  steps:
  - 1
"""
        deals.parse(yaml.safe_load(data)["deals"], config.Config())
        with self.assertLogs() as lg:
            logging.getLogger().error("Deal name missing, skipped")
        self.assertEqual(lg.output, ["ERROR:root:Deal name missing, skipped"])

    def test_missing_strategy(self):
        data = """
deals:
- name: no_strategy
  steps:
  - 1
"""
        deals.parse(yaml.safe_load(data)["deals"], config.Config())
        with self.assertLogs() as lg:
            logging.getLogger().error("Strategy missing for deal no_strategy")
        self.assertEqual(
            lg.output, ["ERROR:root:Strategy missing for deal no_strategy"]
        )

    def test_get_deals(self):
        d = deals.OpenDeals(config.Config(), MockupConnector())
        d.get()
        self.assertEqual(2, len(d))


class TestDefinedDeals(unittest.TestCase):
    def setUp(self) -> None:
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write(base_config)
        f.close()
        self.cfg = config.Config(config_file=f.name)
        self.cfg.open()
        self.definedDeals = deals.DefinedDeals(self.cfg)

        self.activeDeals = deals.Deals(self.cfg)
        for item in test_deals:
            self.activeDeals.append(
                deal.Deal(
                    item["bot_name"],
                    tp=item["take_profit"],
                    deal_id=item["deal_id"],
                    current_so=item["so"],
                )
            )

    def test_find_deal(self):
        ada_deal = self.activeDeals.find(self.definedDeals[0])
        self.assertEqual(ada_deal.name, "ADA/BUSD")

    def test_find_no_matching_deal(self):
        eth_deal = self.activeDeals.find(self.definedDeals[1])
        self.assertIsNone(eth_deal)

    def test_invalid_scaling(self):
        deal_names = []
        for d in self.definedDeals:
            deal_names.append(d.name)
        self.assertIn("scaling_override1_valid", deal_names)
        # non numeric scaling
        self.assertNotIn("scaling_override1_invalid", deal_names)
        # negative scaling, some people want this
        self.assertIn("scaling_override2_invalid", deal_names)

    def test_apply_strategy(self):
        self.modifiedDeals = deals.Deals(self.cfg)
        for i in range(len(self.definedDeals)):
            d = self.activeDeals.find(self.definedDeals[i])
            if d is not None:
                d.modify(self.cfg.strategies[self.definedDeals[i].strategy])
                self.modifiedDeals.append(d)
        self.assertEqual(self.modifiedDeals[0].new_tp, 3)
        self.assertEqual(self.modifiedDeals[1].new_tp, 1)
        self.assertEqual(self.modifiedDeals[2].new_tp, 4)
        self.assertEqual(self.modifiedDeals[3].new_tp, 3)
