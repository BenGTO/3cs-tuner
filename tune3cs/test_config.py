import unittest
import tempfile
import logging
import yaml
from yaml.scanner import ScannerError
import tune3cs.config as config

missing_strategies = """---
deals:
  - name: test
"""
missing_deals = """---
strategies:
  - name: std
"""
no_valid_strategies = """
deals:
  - name: test
strategies:
  - name: test
    type: invalid
"""
no_valid_deals = """
deals:
  - name: test
    disabled: 1
strategies:
  - name: test
    type: discrete
    steps:
      - 1
"""
no_strategy_in_deal = """
deals:
  - name: test
strategies:
  - name: test
    type: discrete
"""


class TestValidation(unittest.TestCase):
    def test_no_deals(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        cfg = config.Config
        f.write(no_strategy_in_deal)
        f.close()
        cfg = config.Config(f.name)

        data = yaml.safe_load(missing_deals)
        cfg.validate(data)
        with self.assertLogs() as lg:
            logging.getLogger().warning("No deals defined")
        self.assertEqual(lg.output, ["WARNING:root:No deals defined"])

    def test_no_strategies(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        cfg = config.Config
        f.write(no_strategy_in_deal)
        f.close()
        cfg = config.Config(f.name)

        data = yaml.safe_load(missing_deals)
        cfg.validate(data)
        with self.assertLogs() as lg:
            logging.getLogger().error("No strategies defined")
        self.assertEqual(lg.output, ["ERROR:root:No strategies defined"])


class ReadConfig(unittest.TestCase):
    def test_file_missing(self):
        cfg = config.Config(config_file="not_existing")
        self.assertRaises(FileNotFoundError, cfg.open)
        with self.assertLogs() as lg:
            logging.getLogger().error("Configuration file not found")
        self.assertEqual(lg.output, ["ERROR:root:Configuration file not found"])

    def test_file_bad_yaml(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write("this is not valid YAML: not : this")
        f.close()
        cfg = config.Config(config_file=f.name)
        self.assertRaises(ScannerError, cfg.open)
        with self.assertLogs() as lg:
            logging.getLogger().error("Configuration file not in YAML format")
        self.assertEqual(
            lg.output, ["ERROR:root:Configuration file not in YAML format"]
        )
        f.close()

    def test_missing_strategies(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write(missing_strategies)
        f.close()
        cfg = config.Config(config_file=f.name)
        self.assertRaises(KeyError, cfg.open)
        with self.assertLogs() as lg:
            logging.getLogger().error("No strategies defined")
        self.assertEqual(lg.output, ["ERROR:root:No strategies defined"])
        f.close()

    def test_no_strategy_in_deal(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write(no_strategy_in_deal)
        f.close()
        ft = open(f.name)
        s = ft.read()

        cfg = config.Config(config_file=f.name)
        with self.assertLogs() as lg:
            logging.getLogger().warning("Deal test has no strategy set, skipping")
        self.assertEqual(
            lg.output, ["WARNING:root:Deal test has no strategy set, skipping"]
        )
        f.close()

    def test_missing_deals(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write(missing_deals)
        f.close()
        ft = open(f.name)
        s = ft.read()

        cfg = config.Config(config_file=f.name)
        with self.assertLogs() as lg:
            logging.getLogger().warning("No deals defined")
        self.assertEqual(lg.output, ["WARNING:root:No deals defined"])
        f.close()

    def test_no_valid_strategy(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write(no_valid_strategies)
        f.close()
        cfg = config.Config(config_file=f.name)
        self.assertRaises(Exception, cfg.open)
        with self.assertLogs() as lg:
            logging.getLogger().error(
                "No applicable strategy found, check strategy types"
            )
        self.assertEqual(
            lg.output, ["ERROR:root:No applicable strategy found, check strategy types"]
        )
        f.close()

    def test_no_valid_deals(self):
        f = tempfile.NamedTemporaryFile(mode="w", delete=False)
        f.write(no_valid_deals)
        f.close()
        cfg = config.Config(config_file=f.name)
        cfg.open()
        with self.assertLogs() as lg:
            logging.getLogger().warning("No applicable deals found (all disabled?)")
        self.assertEqual(
            lg.output, ["WARNING:root:No applicable deals found (all disabled?)"]
        )
        f.close()
