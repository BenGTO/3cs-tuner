import os
import yaml
from yaml.scanner import ScannerError
import logging
import tune3cs.deals
import tune3cs.strategy as strategies


class Config(object):
    """
    Handles the configuration file containing strategies and deal settings
    """

    config_file = "config.yml"
    api_key = ""  # nosec
    api_secret = ""  # nosec
    request_timeout = 30
    nr_of_retries = 5
    retry_status_codes = [500, 502, 503, 504]
    deals = []
    strategies = {}

    def __init__(self, config_file=""):
        """
        Prepare configuration
        :param config_file: default configuration file to use
        """
        self.api_key = os.getenv("TUNE3CS_API_KEY")
        self.api_secret = os.getenv("TUNE3CS_API_SECRET")
        filename = os.getenv("TUNE3CS_CONFIG", config_file)
        if not filename == "":
            self.config_file = filename  # pragma: no cover

    def validate(self, data):
        """
        Validation of configuration file, after a valid YAML has been parsed. Needed attributes:

        - list of deals
        - list of strategies

        :param data: configuration structure read from YAML configuration file
        :return: None; raises an error if validation fails
        """
        if "deals" not in data.keys():
            logging.warning("No deals defined")
        if "strategies" not in data.keys():
            logging.error("No strategies defined")
            raise KeyError

    def open(self, config_file=""):
        """
        Read and parse configuration file
        :param config_file: optional overwrite of default configuration file
        :return: None
        """
        # Apply configuration file overwrite, if set
        config_file = (
            self.config_file
            if config_file is None or config_file == ""
            else config_file
        )
        logging.debug("Looking for configuration file {0}".format(config_file))
        # open configuration file
        try:
            f = open(config_file)
        except FileNotFoundError as e:  # noqa: F841
            logging.error("Configuration file not found")
            raise e

        # parse file YAML structure
        try:
            data = yaml.safe_load(f)
        except ScannerError as e:  # noqa: F841
            logging.error("Configuration file not in YAML format")
            raise e

        # File successfully opened, YAML is ok.
        # Parse structure and build object trees
        try:
            self.validate(data)
        except Exception as e:
            raise e
        self.deals = []
        self.strategies = {}

        # Read strategies listed
        self.strategies = strategies.parse(data["strategies"])

        if len(self.strategies) == 0:
            logging.error("No applicable strategy found, check strategy types")
            raise Exception

        # Read deals to be tuned
        tune3cs.deals.parse(data["deals"], self)
        if len(self.deals) == 0:
            logging.warning("No applicable deals found (all disabled?)")
